direction = c("Up", "Down", "Left", "Right", "Left", "Up")
factorDirection = factor(direction)

is.factor(factorDirection)

levels(x=factorDirection) # ein Factor Object beinhaltet levels in welche alle möglichen Werte gespeichert werden

day.of.the.week = c("Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag")
arbeitstage = c("Dienstag", "Donnerstag", "Montag")
arbeitstageFactor = factor(x=arbeitstage, levels=day.of.the.week, ordered=T)
arbeitstageFactor
