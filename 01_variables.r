## Variable names need to start with a letter and can contain numbers, undescore or dots
my_var <- 4
my.var = 5

## Print out the different information of data that you can store
print(class(4)) # number, numeric, integer
print(class(4L))
print(class(TRUE)) # booleans
print(class(T))
print(class(FALSE))
print(class(F))
print(class(1 + 4i)) # complex data type
print(class("Sample")) # string, Zeichenkette
print(class(charToRaw("Sample"))) # raw

## predicates -- Behauptungen returnen true oder false
is.integer()
is.numeric()
is.matrix()
is.dataframe()
is.vector()
is.character()

## convert to different data types
as.integer()
# ...
