## DataFrames sind Tabellen so wie Excel Tabellen

customerData = data.frame(name=c("Tom", "Sally", "Sue"),
                          age=c(34, 42, 65),
                          stringsAsFactors=F)

customerData[1, 1]
customerData[1,1:2]
customerData[1:3, 2]
dim(customerData) # get dimensions

newRecord = data.frame(name="Marc", age=25)
customerData = rbind(customerData, newRecord)

debt = c(0, 25, 46, 38.98)
customerData = cbind(customerData, debt)

customerData[customerData$debt > 0,] # warum komma?
