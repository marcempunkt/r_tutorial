numbers <- c(3, 2, 0, 1, 8)
numbers

numbers[1] # to get the first item of a vector
length(numbers) # get length of a vector
numbers[length(numbers)] # get last item of a vector
numbers[-1] # alles außer das erste
numbers[c(1, 2)] # gette nur die ersten beiden
numbers[2:4] # gette eine range
numbers[5] = 1 # ersteze/überschreibe eine Stelle
numbers[c(4, 5)] = 2 # ersetze/überschreibe 4 und 5

sort(numbers, decreasing=TRUE) # sortiere numbers von groß zu klein. Erstellt dabei ein neuen Vector

oneToTen = 1:10 # range von 1 - 10
add3 = seq(from = 3, to = 27, by = 3) # range von 3 bis 27 wo immer 3 addiert wird
evens = seq(from = 2, by = 2, length.out = 10) # erstelle range von 10 geraden Zielen ab 2 an

4 %in% evens # checke ob 4 in vector ist

rep(x=2, times=5, each=2) # repeate x 5 mal bei jeden step 2 mal
rep(x=c(1, 2, 3), times=2, each=2)


