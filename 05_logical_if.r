T && F
T || F
!T

age = 18

if (age >= 18) {
    print("Drive and vote")
} else if (age >= 16) {
    print("Drive")
} else {
    print("Wait")
}

grade = "Z"

switch(grade,
       "A" = print("Great"),
       "B" = print("Good"),
       "C" = print("Ok"),
       "D" = print("Bad"),
       "F" = print("Terrible")
       print("No Such Grade"))
